Using the menu system:
1- Create a menu with machine name 'personalities' 
	-Add links to this menu, these items will show up on the top most bar
2- Create new menus for every item that was created on the personalities menu, these new menus
	show up underneath the top most bar.
	-Use the same name as the item in personalities menu with requirements:
	-remove space between words, add 'sub' to the end
		
		
Example:
1) Make Personalities menu:
	machine name: ' personalities '
	Title: Personalities (can be anything)
	Add items:	- First Personality
					- Second
	
2) Make new menu for each of the created personalities:

	machine name: ' firstpersonalitysub '
	title: First Personality submenu (can be anything)

	machine name: ' secondsub '
	title: Second submenu (can be anything)
	
	
	
*NOTES*:
-After installing theme create personalities bar, or else the bar will not show.

-Enable logo in themes options, upload or choose path , make sure to keep same dimensions as logo.png (466x 128)

-if submenus have many items and you resize the window smaller the links will wrap (set body min-width in css accordingly)

-Links in the personality bar should be unique


-------------------------------------
Ignore  the rest for now , this is from drupal 6 
-Using thrifty404 for custom error page, without custom page the page elements won't render, personality links and submenu links will not render and cause the page to look broken

-Setting up Panels: for every new panels page check the machine name, and create a page-page-panelmachinename.tpl.php file. (copy/paste from existing panel page template in knowpulse_theme folder), need these files in order to get rid of panel borders, templates with that naming has same template as default page.tpl but is also using the front-page.css.

