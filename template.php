<?php

/**
 * Implements hook_preprocess_views_view().
 * Add a collapsible fieldset around the views exposed filters
 */
function kptheme_preprocess_views_view(&$variables) {

  // No fieldset for the following views
  $skip_views = array('kp_genotype_search');
  $skip_view = FALSE;
  if (in_array($variables['view']->name, $skip_views)) {
    $skip_view = TRUE;
  }

  // Wrap exposed filters in a fieldset.
  $default_display = $variables['view']->display['default'];
  if ($variables['exposed'] AND !($skip_view)) {


    // We want it collapsed by default only if there are search results
    $collapsed = FALSE;
    $classes = array('collapsible');
    if (sizeof($variables['view']->result) > 0) {
      $collapsed = TRUE;
      $classes = array('collapsible', 'collapsed');
    }

    // Ensure required js libs are added
    drupal_add_js('misc/form.js');
    drupal_add_js('misc/collapse.js');
      // Build fieldset element, using correct array nesting for theme_fieldset
      $fieldset['element'] = array(
        '#title' => t('Search Criteria'),
        '#collapsible' => TRUE,
        '#collapsed' => $collapsed,
        '#attributes' => array('class' => $classes),
        '#value' => $variables['exposed'],
        '#children' => '',
      );
      // Reassign exposed filter tpl var to fieldset value
      $variables['exposed'] = theme('fieldset', $fieldset);
  }

}



/**
 * Implementation of THEMENAME_preprocess_page().
 * Required to access personalities in template files
 */
function kptheme_preprocess_page(&$vars) {
  // Load custom links menu into a variable called personalities, the name is always prefixed with 'menu-'
  $vars['personalities'] = menu_navigation_links('menu-personalities');

  //create subname variable for all personalities menu items
  foreach ( $vars['personalities'] as $personas) {
    $subname = drupal_strtolower($personas['title']) . "sub" ;
	  $subname =  str_replace(" ", "", $subname); //in case the main title has a space
    $menusubname="menu-" . $subname;
    $vars[$subname] = menu_navigation_links($menusubname);
  }
  // if this is a panel page, add template suggestions
  if (module_exists('panels')) {
    if($panel_page = page_manager_get_current_page()) {
      // add a generic suggestion for all panel pages
      $suggestions[] = 'page-panel';
      // add the panel page machine name to the template suggestions
      $suggestions[] = 'page-' . $panel_page['name'];
      // merge the suggestions in to the existing suggestions (as more specific than the existing suggestions)
      $vars['template_files'] = array_merge($vars['template_files'], $suggestions);
    }
  }

}